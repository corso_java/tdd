package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import model.Person;
import source.FilePersonSource;
import source.IPersonSource;

class TestSimple {

	@Test
	void test() {
		int expected = 4;
		int sum = 2 + 2;
		assertTrue(expected == sum, "La somma non è stata calcolata correttamente");
	}

	@Test
	void test2() {
		int expected = 4;
		int a = 2;
		int b = 2;
		CalculateSum tool = new CalculateSum();
		int result = tool.calculate(a, b);
		assertTrue(expected == result, "La somma non è stata calcolata correttamente");
	}

	@Test
	void test3() {
		IPersonSource source = new FilePersonSource();
		List<Person> people = source.retrieve();
		Person expected = new Person("Mario", "Rossi", 18);
		assertTrue(people != null, "La lista non è stata creata");
		assertTrue(people.size()>0,"Non ci sono persone");
		Person test = people.get(0);
		boolean result = expected.equals(test);
		assertTrue(result, "Le due persone non sono le stesse");
	}
}
