package source;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {

	private List<String> risultato;

	public List<String> readFile(String file) {
		if (risultato == null) {
			risultato = new ArrayList<>();
		} else {
			risultato.clear();
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
			String riga = reader.readLine();
			while (riga != null) {
				risultato.add(riga);
				riga = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Errore durante l'apertura del file !");
		}
		return risultato;
	}
}
