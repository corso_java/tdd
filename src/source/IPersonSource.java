package source;

import java.util.List;

import model.Person;

public interface IPersonSource {

	public List<Person> retrieve();

}
