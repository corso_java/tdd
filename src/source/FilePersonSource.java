package source;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import model.Person;

public class FilePersonSource implements IPersonSource {

	@Override
	public List<Person> retrieve() {
		List<String> file = new ReadFile().readFile("./people.txt");
		List<Person> people = new ArrayList<>();
		for (String row : file) {
			StringTokenizer stringTokenizer = new StringTokenizer(row, "|");
			String name = stringTokenizer.nextToken();
			String surname = stringTokenizer.nextToken();
			int age = Integer.valueOf(stringTokenizer.nextToken());
			people.add(new Person(name, surname, age));
		}
		return people;
	}
}
